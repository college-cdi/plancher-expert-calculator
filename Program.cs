﻿using PlancherExpertCalculator.Structs;

namespace PlancherExpertCalculator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Initialises les variables.
            Materials[] Materials = new Materials[5];
            Materials[0] = new Materials { Type = "Tapis commercial", MaterialRate = 1.29, LaborRate = 2.00 };
            Materials[1] = new Materials { Type = "Tapis de qualité", MaterialRate = 3.99, LaborRate = 2.25 };
            Materials[2] = new Materials { Type = "Plancher de bois franc", MaterialRate = 3.49, LaborRate = 3.25 };
            Materials[3] = new Materials { Type = "Plancher flottant", MaterialRate = 1.99, LaborRate = 2.25 };
            Materials[4] = new Materials { Type = "Céramique", MaterialRate = 1.49, LaborRate = 3.25 };

            string userInput = "Y";

            // Offre la possibilité de réessayer le programme sans avoir à quitter et relancer.
            while (userInput.ToUpper() == "Y")
            {
                PrintMaterials(Materials);

                Materials selectedMaterial = getMaterialsInput(Materials);
                Area userArea = GetAreaDimensions();

                Invoice invoice = createInvoice(selectedMaterial.Type, userArea);

                invoice.Show();

                Console.WriteLine($"Votre total est donc de {String.Format("{0:0.00}$",invoice.totalPrice)} pour une surface de {userArea.Width} pieds par {userArea.Length} pieds totalant une aire de {userArea.SurfaceArea} pied² en {selectedMaterial.Type.ToLower()}.");

                Console.ReadKey();

                Console.Write("Souhaitez-vous relancer l'application? Appuyer sur Y pour recommencer ou N pour terminer. Si vous n'entrez aucun charactère, l'application quittera par défaut. \n[Y/N]:");
                userInput = Console.ReadLine() ?? "N";

            }
            
            Console.WriteLine("Vous allez maintenant quitter le programme. Appuyer sur une touche pour continuer.");
            Console.ReadKey();
        }

        public static void PrintMaterials(Materials[] Materials)
        {
            Int16 index = 1;

            Console.WriteLine("Quel type de matériel voulez-vous pour votre plancher?");
            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine($"| [0] - {"Choix de matériel".PadRight(25, ' ')} | {"Prix du matériel"} {"|".PadLeft(8, ' ')} {"Prix de la main d'oeuvre"} |");
            Console.WriteLine("----------------------------------------------------------------------------------------");

            foreach (Materials material in Materials)
            {
                Console.WriteLine($"| [{index}] - {material.Type.PadRight(25, ' ')} | {material.MaterialRate}$/{material.Unit.PadRight(17, ' ')} | {String.Format("{0:0.00}", material.LaborRate).PadLeft(18, ' ')} $/{material.Unit} |");
                index += 1;
            }
            Console.WriteLine("----------------------------------------------------------------------------------------");

        }

        // Get user input
        public static Materials getMaterialsInput(Materials[] Materials)
        {
            string? input = "";
            string confirmationInput = "N";
            bool isInputConform = false;

            while (!isInputConform || confirmationInput != "Y")
            {
                if (input == "?" || confirmationInput == "?")
                {
                    PrintMaterials(Materials);
                }

                Console.WriteLine("Appuyer sur une touche [1-5] pour sélectionner votre choix.");
                Console.WriteLine("Si vous souhaitez revoir les options, vous pouvez entrer le charactère \"?\" à tout moment.");

                input = Console.ReadLine();

                if (!String.IsNullOrEmpty(input) && input.Length >= 1 && Char.IsDigit(input, 0) && Convert.ToInt16(input) > 0 && Convert.ToInt16(input) <= 5)
                {
                    Materials selectedMaterial = Materials[Convert.ToInt16(input) - 1];  /* best way to get selectedMaterials */
                    Console.Write($"Vous avez sélectionner le matériel {selectedMaterial.Type} au taux de {selectedMaterial.MaterialRate}$ le pied carré et la main d'oeuvre au taux de {selectedMaterial.LaborRate}$ le pied carré. \n" +
                    "Confirmez-vous ce choix?\n[Y/N]:");
                    confirmationInput = Console.ReadLine() ?? "N";
                    confirmationInput = confirmationInput.ToUpper();
                    isInputConform = true;
                } else
                {
                    Console.WriteLine("Vous devez entrer un nombre valide entre 1 et 5 pour continuer. Si vous souhaitez quitter, simplement appuyer sur Ctrl+C.\n\n");
                }
            }
            return Materials[Convert.ToInt16(input) - 1];
        }

        public static Int16 GetAreaWidth()
        {
            string? areaWidth = "";
            bool isNumeric = false;

            Console.WriteLine("Veuillez entrer la largeur de votre plancher en pied.");

            while (String.IsNullOrEmpty(areaWidth) || areaWidth.Length < 1 || !isNumeric)
            {
                areaWidth = Console.ReadLine();
                isNumeric = int.TryParse(areaWidth, out int _width);
                if ((!isNumeric) || (_width <= 0))
                {
                    Console.WriteLine("Vous devez entrez un nombre valide. Quelle est la largeur de votre plancher?");
                    areaWidth = null;
                }
            }

            return Convert.ToInt16(areaWidth);
        }

        public static Int16 GetAreaLength()
        {
            string? areaLength = "";
            bool isNumeric = false;

            Console.WriteLine("Veuillez entrer la longueur de votre plancher en pied.");

            while (String.IsNullOrEmpty(areaLength) || areaLength.Length < 1 || !isNumeric)
            {
                areaLength = Console.ReadLine();
                isNumeric = Int16.TryParse(areaLength, out Int16 _length);
                if ((!isNumeric) || (_length <= 0))
                {
                    Console.WriteLine("Vous devez entrez un nombre valide. Quelle est la largeur de votre plancher?");
                    areaLength = null;
                }
            }

            return Convert.ToInt16(areaLength);
        }

        public static Area GetAreaDimensions()
        {
            string confirmationInput = "N";
            Area userArea = new Area();

            while (confirmationInput != "Y")
            {
                userArea.Width = GetAreaWidth();
                userArea.Length = GetAreaLength();

                Console.WriteLine($"La surface couverte est donc de {userArea.Width}\' par {userArea.Length}\' pour un total de {userArea.SurfaceArea} pieds²");
                Console.Write($"Les mesures sont-elles exactes? Appuyer sur \"Y\" pour continuer.\n[Y/N]:");
                confirmationInput = Console.ReadLine() ?? "N";
                confirmationInput = confirmationInput.ToUpper();
            }
            return userArea;
        }

        /* Initialises un objet de type Invoice calcule et assigne les valeurs : prix finale de la main d'oeuvre et prix final
         * du matériel. Le total est compris dans une méthode appelé "TotalPrice" qui retourne LaborPrice + MaterialPrice
         */
        public static Invoice createInvoice(string selectedMaterial, Area userArea)
        {
            Invoice invoice = new Invoice();

            /* Switch pas très utile, mais rajouté pour répondre aux critères du projets.
                Meilleur solution : 
                    Invoice invoice = new Invoice();
                    invoice.materialPrice = userArea.SurfaceArea * selectedMaterial.MaterialRate;
                    invoice.laborPrice = userArea.SurfaceArea * selectedMaterial.LaborRate;
            */
            switch (selectedMaterial)
            {
                case "Tapis commercial":
                    invoice.materialPrice = userArea.SurfaceArea * 1.29;
                    invoice.laborPrice = userArea.SurfaceArea * 2.00;
                    break;

                case "Tapis de qualité":
                    invoice.materialPrice = userArea.SurfaceArea * 3.99;
                    invoice.laborPrice = userArea.SurfaceArea * 2.25;
                    break;

                case "Plancher de bois franc":
                    invoice.materialPrice = userArea.SurfaceArea * 3.49;
                    invoice.laborPrice = userArea.SurfaceArea * 3.25;
                    break;


                case "Plancher flottant":
                    invoice.materialPrice = userArea.SurfaceArea * 1.99;
                    invoice.laborPrice = userArea.SurfaceArea * 2.25;
                    break;

                case "Céramique":
                    invoice.materialPrice = userArea.SurfaceArea * 1.49;
                    invoice.laborPrice = userArea.SurfaceArea * 3.25;
                    break;

                default:
                    invoice.materialPrice = 0.00;
                    invoice.laborPrice = 0.00;
                    break;
            }

            return invoice;
        }

    }
}