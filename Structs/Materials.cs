﻿namespace PlancherExpertCalculator.Structs
{
    struct Materials {
        public string Type;
        public double MaterialRate;
        public double LaborRate;
        public string Unit // Sera toujours la même unité pour les calculs, facilitera les messages "Display"
        {
            get
            {
            return "pi²";
            }
        }
    }
}
