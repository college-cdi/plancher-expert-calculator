﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlancherExpertCalculator.Structs
{
     struct Area
    {
        public Int16 Width;
        public Int16 Length;
        public Int32 SurfaceArea {
            get
            {
                return this.Width * this.Length;
            }
        }
    }
}
