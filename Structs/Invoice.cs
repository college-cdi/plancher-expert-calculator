﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlancherExpertCalculator.Structs
{
     struct Invoice
    {
        public double laborPrice;
        public double materialPrice;
        public double totalPrice
        {
            get
            {
                return this.laborPrice + this.materialPrice;
            }
        }

        public string Show()
        {
                Console.WriteLine("\n\n\n" +
                              "                                 Facture");
                Console.WriteLine("                    ----------------------------------");
                Console.Write("                    | Matériel:");
                Console.WriteLine($"{String.Format("{0:0.00}", materialPrice),20}$ |");
                Console.Write("                    | Main d'oeuvre: ");
                Console.WriteLine($"{String.Format("{0:0.00}", laborPrice),14}$ |");
                Console.Write("                    | Total:");
                Console.WriteLine($"{String.Format("{0:0.00}", totalPrice),23}$ |");
                Console.WriteLine("                    ----------------------------------");
                return "";
        }
    }
}
